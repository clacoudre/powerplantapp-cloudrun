from django.contrib import sitemaps

class MainSitemap(sitemaps.Sitemap):

	def items(self):
		return [self]

	location = "/"
	changefreq = "monthly"
	priority = "1"