from django.apps import AppConfig


class PowerplantappConfig(AppConfig):
    name = 'powerplantapp'
