var customMap = function(map){    

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://opendatacommons.org/licenses/odbl/summary/">ODbL</a>, © <a href="https://www.mapbox.com/">Mapbox</a>',
tileSize: 512,
maxZoom: 18,
zoomOffset: -1,
id: 'mapbox/light-v10',
accessToken: 'pk.eyJ1IjoiY2xhY291ZHJlIiwiYSI6ImNraWFzM3F1czAxODAyeG80bHYyYjEyZGkifQ.9K_EnEVIrUEIjkQrmovkgQ'
}).addTo(map);

/*Legend specific*/
var legend = L.control({ position: "bottomleft" });
legend.onAdd = function(map) {
  var div = L.DomUtil.create("div", "legend");
  div.innerHTML += "<h4>Primary Energy</h4>";
  div.innerHTML += '<i style="background: #0f0f0f"></i><span>Oil</span><br>';
  div.innerHTML += '<i style="background: #848388"></i><span>Coal</span><br>';
  div.innerHTML += '<i style="background: #5ebac9"></i><span>Gas</span><br>';
  div.innerHTML += '<i style="background: #477AC2"></i><span>Hydro</span><br>';
  div.innerHTML += '<i style="background: #448D40"></i><span>Biomass</span><br>';
  div.innerHTML += '<i style="background: #015955"></i><span>Cogeneration</span><br>';
  div.innerHTML += '<i style="background: #fcf876"></i><span>Nuclear</span><br>';
  div.innerHTML += '<i style="background: #E6E696"></i><span>Waste</span><br>';
  div.innerHTML += '<i style="background: #fdb353"></i><span>Solar</span><br>';
  div.innerHTML += '<i style="background: #B22222"></i><span>Geothermal</span><br>';
  div.innerHTML += '<i style="background: #9A6445"></i><span>Storage</span><br>';
  div.innerHTML += '<i style="background: #bad5ff"></i><span>Wind</span><br>';
  div.innerHTML += '<i style="background: #00416A"></i><span>Wave and Tidal</span><br>';

  return div;
};

legend.addTo(map);

};


var addCentroids = function(centroidsLayer, lat, long,  hydro, gas, oil, biomass, wind, nuclear, coal, solar, waste, wave, petcoke, geothermal, cogeneration, storage, other){

max_radius = 60;
mw_0 = 180000;
k = 1/80000;
min_radius = 8;
var sum_mw = 
hydro + 
gas + 
oil + 
biomass +
wind + 
nuclear + 
coal +
solar + 
waste  + 
wave +
petcoke + 
geothermal +
cogeneration + 
storage + 
other;

var radius_sigmoid = (max_radius-min_radius)/(1 + Math.exp(-k*(sum_mw - mw_0))) + min_radius;


var mw_log = Math.log(sum_mw);
var options = {
    radius: radius_sigmoid,
    fillOpacity: 0.8,
    data: {
        'Oil': oil,
        'Coal': coal,
        'Gas': gas,
        'Hydro': hydro,
        'Biomass': biomass,
        'Wind': wind,
        'Nuclear': nuclear,
        'Solar': solar,
        'Waste': waste,
        'Wave and Tidal': wave,
        'Petcoke': petcoke,
        'Geothermal': geothermal,
        'Cogeneration': cogeneration,
        'Storage': storage,
        'Other': other,

    },
    chartOptions: {
        'Hydro': {
            fillColor: '#477AC2',
            minValue: 0,
            maxValue: 20,
            maxHeight: 20,
            displayName: 'Hydro',
            displayText: function (value) {
                return value.toFixed(0) + ' MW';
            }
        },
        'Oil': {
            fillColor: '#0f0f0f',
            minValue: 0,
            maxValue: 20,
            maxHeight: 20,
            displayName: 'Oil',
            displayText: function (value) {
                return value.toFixed(0) + ' MW';
            }
        },
        'Coal': {
            fillColor: '#848388',
            minValue: 0,
            maxValue: 20,
            maxHeight: 20,
            displayName: 'Coal',
            displayText: function (value) {
                return value.toFixed(0) + ' MW';
            }
        },
        'Gas': {
            fillColor: '#5ebac9',
            minValue: 0,
            maxValue: 20,
            maxHeight: 20,
            displayName: 'Gas',
            displayText: function (value) {
                return value.toFixed(0) + ' MW';
            }
        },
        'Biomass': {
            fillColor: '#448D40',
            minValue: 0,
            maxValue: 20,
            maxHeight: 20,
            displayName: 'Biomass',
            displayText: function (value) {
                return value.toFixed(0) + ' MW';
            }
        },
        'Wind': {
            fillColor: '#bad5ff',
            minValue: 0,
            maxValue: 20,
            maxHeight: 20,
            displayName: 'Wind',
            displayText: function (value) {
                return value.toFixed(0) + ' MW';
            }
        },
        'Nuclear': {
            fillColor: '#fcf876',
            minValue: 0,
            maxValue: 20,
            maxHeight: 20,
            displayName: 'Nuclear',
            displayText: function (value) {
                return value.toFixed(0) + ' MW';
            }
        },
        'Solar': {
            fillColor: '#fdb353',
            minValue: 0,
            maxValue: 20,
            maxHeight: 20,
            displayName: 'Solar',
            displayText: function (value) {
                return value.toFixed(0) + ' MW';
            }
        },
        'Waste': {
            fillColor: '#E6E696',
            minValue: 0,
            maxValue: 20,
            maxHeight: 20,
            displayName: 'Waste',
            displayText: function (value) {
                return value.toFixed(0) + ' MW';
            }
        },
        'Wave and Tidal': {
        
        fillColor: '#00416A',
        minValue: 0,
        maxValue: 20,
        maxHeight: 20,
        displayName: 'Wave and Tidal',
        displayText: function (value) {
            return value.toFixed(0) + ' MW';
        }
        },
        'Petcoke': {
            fillColor: '#0f0f0f',
            minValue: 0,
            maxValue: 20,
            maxHeight: 20,
            displayName: 'Petcoke',
            displayText: function (value) {
                return value.toFixed(0) + ' MW';
            }
        },
        'Geothermal': {
            //color: '#B22222',
            fillColor: '#B22222',
            minValue: 0,
            maxValue: 20,
            maxHeight: 20,
            displayName: 'Geothermal',
            displayText: function (value) {
                return value.toFixed(0) + ' MW';
            }
        },
        'Cogeneration': {
            fillColor: '#015955',
            minValue: 0,
            maxValue: 20,
            maxHeight: 20,
            displayName: 'Cogeneration',
            displayText: function (value) {
                return value.toFixed(0) + ' MW';
            }
        },
        'Storage': {
            fillColor: '#9A6445',
            minValue: 0,
            maxValue: 20,
            maxHeight: 20,
            displayName: 'Storage',
            displayText: function (value) {
                return value.toFixed(0) + ' MW';
            }
        },
        'Other': {
            fillColor: '#FFFFFF',
            minValue: 0,
            maxValue: 20,
            maxHeight: 20,
            displayName: 'Other',
            displayText: function (value) {
                return value.toFixed(0) + ' MW';
            }
        },
    },
    weight: 0.2,
};

var pieMarker = new L.PieChartMarker(new L.LatLng(lat, long), options);
centroidsLayer.addLayer(pieMarker);
    return centroidsLayer
};

var addPlants = function(geojson_data){

function getRadius(capacity_mw){
    return 300*Math.sqrt(capacity_mw)
};

function getColor(primary_fuel){
switch(primary_fuel) {
    case 'Hydro':
        color = '#477AC2';
        break;
    case 'Gas':
        color = '#5ebac9';
        break;
    case 'Oil':
        color = '#0f0f0f';
        break;
    case 'Biomass': 
        color = '#448D40';
        break;
    case 'Wind': 
        color = '#bad5ff';
        break;
    case 'Nuclear': 
        color = '#fcf876';
        break;
     case 'Coal':
        color = '#848388';
        break;
    case 'Solar':
        color = '#fdb353';
        break;
    case 'Waste': 
        color = '#E6E696';
        break;
    case 'Wave and Tidal': 
        color = '#00416A';
        break;
    case 'Petcoke': 
        color = '#0f0f0f';
        break;   
    case 'Geothermal': 
        color = '#B22222';
        break;
    case 'Cogeneration': 
        color = '#015955';
        break;
    case 'Storage': 
        color = '#9A6445';
        break;                                                       
    default: 
        color = 'white';
        break;
}
    return color
    };

    var plants = L.geoJson(geojson_data, {

    pointToLayer: function(feature, latlng) {
        return new L.Circle(latlng, {radius: getRadius(feature.properties.capacity_mw), fillOpacity: 0.8, opacity: 0.8, color: getColor(feature.properties.primary_fuel), fillColor:getColor(feature.properties.primary_fuel)});
    },
    onEachFeature: function(feature, layer) {
        var popupcontent = "<dl><dt>Name</dt>"
           + "<dd>" + feature.properties.plant_name + "</dd>"
           + "<dt>Primary Energy</dt>"
           + "<dd>" + feature.properties.primary_fuel + "</dd>"
           + "<dt>Power</dt>"
           + "<dd>" + feature.properties.capacity_mw.toFixed(0) + " MW" + "</dd>"
           + "<dt>Commissioning Year</dt>"
           + "<dd>" + feature.properties.commissioning_year + "</dd></dl>"
        layer.bindPopup(popupcontent);
    },
    filter: function(feature, layer) {
        return (!feature.properties.plant_name.startsWith('Itaipu ('))
    }})
    
    return plants};