from django.shortcuts import render
from django.views import generic
from django.contrib.gis.geos import Point
from django.db.models import Sum
from .models import PowerPlant, CountryCentroids
from django_pivot.pivot import pivot
from django.core import serializers

# Create your views here.



class Home(generic.ListView):

	context_object_name = 'powerplant_list'

	def get_template_names(self):
		if self.request.session.get('visited', False):
			return ['powerplantapp/index.html']
		else:
			self.request.session['visited'] = True
			return ['powerplantapp/index_w.html']

	def get_queryset(self):
		return PowerPlant.objects.all().order_by('-capacity_mw')

	def get_context_data(self, **kwargs):
		context = super(Home, self).get_context_data(**kwargs)
		context['plant_data'] = serializers.serialize('geojson', self.get_queryset(), geometry_field='location', fields=('primary_fuel', 'plant_name', 'capacity_mw', 'commissioning_year',))
		context['statistics_by_country'] = PowerPlant.objects.values('country','flag_id').annotate(sum_gw = Sum('capacity_mw')/1000).order_by('-sum_gw')
		
		pivot_stats = pivot(PowerPlant.objects.all() \
			.select_related('country_info'), 
		'country_info__centroid_location' ,'primary_fuel', 'capacity_mw', display_transform = lambda s:s.replace(' ', '_'), default=0)
		context['statistics_by_country_pivot'] = pivot_stats
		return context