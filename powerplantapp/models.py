from django.contrib.gis.db import models

# Create your models here.

class CountryCentroids(models.Model):
	country_code = models.CharField(max_length = 10, unique=True)
	#country_code = models.ForeignKey(PowerPlant, on_delete=models.CASCADE, to_field='country_code')
	centroid_location = models.PointField()

class PowerPlant(models.Model):
	country_info = models.ForeignKey(CountryCentroids, on_delete=models.CASCADE, to_field='country_code', max_length = 10)
	#country_code = models.CharField(max_length = 10, unique=True)
	country = models.CharField(max_length = 50)
	plant_name = models.CharField(max_length = 100)
	flag_id = models.CharField(max_length = 10, default = '')
	plant_id = models.CharField(max_length = 50)
	capacity_mw = models.FloatField()
	location = models.PointField()
	primary_fuel = models.CharField(max_length = 20)
	other_fuel1 = models.CharField(max_length = 20, null=True, blank=True)
	other_fuel2 = models.CharField(max_length = 20, null=True, blank=True)
	other_fuel3 = models.CharField(max_length = 20, null=True, blank=True)
	commissioning_year = models.PositiveSmallIntegerField(null=True, blank=True)
	owner = models.CharField(max_length = 200, null=True, blank=True)
	data_source = models.CharField(max_length = 100, null=True, blank=True)
	url_source = models.CharField(max_length = 1000, null=True, blank=True)
	geolocation_source = models.CharField(max_length = 100, null=True, blank=True)
	wepp_id	= models.CharField(max_length = 40, null=True, blank=True)
	year_of_capacity_data = models.PositiveSmallIntegerField(null=True, blank=True)
	generation_gwh_2013	= models.FloatField(null=True, blank=True)
	generation_gwh_2014	= models.FloatField(null=True, blank=True)
	generation_gwh_2015	= models.FloatField(null=True, blank=True)
	generation_gwh_2016	= models.FloatField(null=True, blank=True)
	generation_gwh_2017	= models.FloatField(null=True, blank=True)
	estimated_generation_gwh = models.FloatField(null=True, blank=True)

	def __str__(self):
		return self.plant_name


