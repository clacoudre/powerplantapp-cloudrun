from django.contrib import admin
from django.contrib.gis.admin import OSMGeoAdmin
from .models import PowerPlant, CountryCentroids
# Register your models here.

@admin.register(PowerPlant)
class PowerPlantAdmin(OSMGeoAdmin):
	list_display = ('country_info', 'country', 'flag_id', 'plant_name', 'capacity_mw', 'primary_fuel', 'location')

@admin.register(CountryCentroids)
class CountryCentroidsAdmin(OSMGeoAdmin):
	list_display = ('country_code', 'centroid_location')