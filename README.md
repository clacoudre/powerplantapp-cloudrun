# About the project
https://powerplant.app/  
Interactive world map of power plants.

## Data Source
World Resource Institute's Global Power Plant Database: https://datasets.wri.org/dataset/globalpowerplantdatabase

## Methodology
World Resources Institute (WRI) aggregated over 600 data sources (national energy agencies, utilities, transmission operators, crowdsourced data, etc). The current database almost includes 30,000 power plants. 
The complete technical note from World Resources Institute can be found [here](https://www.wri.org/publication/global-power-plant-database).

# Built with
- [Django](https://www.djangoproject.com/)
- [PostGIS](https://postgis.net/)
- [Docker](https://www.docker.com/)
- [Google Cloud Run](https://cloud.google.com/run/)
- [Leaflet](https://leafletjs.com/)
